<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Testing\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class TaskTest extends TestCase
{
    use RefreshDatabase;

    public function test_authenticated_users_can_create_a_new_task()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $files[] = File::create('avatar.jpg', 100);

        $response = $this->postJson('/api/tasks', [
            'user_id' => $user->id,
            'files' => $files,
            'title' => 'title test 1',
            'description' => 'description test 1',
            'status_id' => rand(1, 3),
        ]);

        $task = Task::latest()->first();

        $response->assertStatus(201);
        $this->assertEquals($task->title, 'title test 1');
        $this->assertEquals($task->description, 'description test 1');

        $path = "/uploads/$task->id";

        foreach ($files as $file) {
            $this->assertFileEquals($file, Storage::path("$path/$file->name"));
        }
        //cause i am not using fake disks i delete the directory created for the test.
        Storage::deleteDirectory($path);

    }

    public function test_authenticated_users_can_update_a_task()
    {
        $this->actingAs(User::factory()->create(), 'api');

        $task = Task::factory()->create(['user_id' => Auth::id()]);

        $task->title = 'Updated Title';

        $data = $task->toArray();
        $files[] = File::create('avatar2.jpg', 100);
        $data['files'] = $files;

        $response = $this->putJson("/api/tasks/$task->id", $data);

        $response->assertStatus(200);
        $this->assertDatabaseHas('tasks', ['id' => $task->id, 'title' => 'Updated Title']);

        $path = "/uploads/$task->id";
        foreach ($files as $file) {
            //dd("$path/$file->name");
            $this->assertFileEquals($file, Storage::path("$path/$file->name"));
        }
        //cause i am not using fake disks i delete the directory created for the test.
        Storage::deleteDirectory($path);

    }

    public function test_authenticated_users_can_soft_delete_a_task()
    {
        $this->actingAs(User::factory()->create(), 'api');

        $task = Task::factory()->create();

        $response = $this->deleteJson("/api/tasks/$task->id");
        $this->assertSoftDeleted('tasks', [
            'id' => $task->id,
        ]);
        $response->assertStatus(200);
    }

    public function test_authenticated_users_can_hard_delete_a_task()
    {
        $this->actingAs(User::factory()->create(), 'api');

        $task = Task::factory()->create();

        $response = $this->deleteJson("/api/tasks-hard/$task->id");

        $this->assertDatabaseMissing('tasks', ['id' => $task->id]);

        $response->assertStatus(200);
    }
}
