<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_user_can_be_registered()
    {
        $response = $this->postJson('/api/register', [
            'name' => 'Sally',
            'email' => 'admin@admin.com5',
            'password' => 'secret',
        ]);

        $response->assertStatus(201);
        $this->assertCount(1, User::all());
        $user = User::first();
        $this->assertEquals($user->email, 'admin@admin.com5');

    }

    public function test_a_user_can_retrieve_its_tasks()
    {
        $user = User::factory()
            ->has(Task::factory()->count(3))
            ->create();
        $this->actingAs($user, 'api');

        $response = $this->getJson("/api/users/$user->id/tasks");

        $response->assertStatus(200);

        $response->assertJsonCount(3, 'data');
    }

}
