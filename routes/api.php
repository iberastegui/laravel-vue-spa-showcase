<?php

use App\Http\Controllers\RegisterController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserTaskController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
Route::post('/register', [RegisterController::class, 'create']);

Route::group(['middleware' => 'auth:api'], function () {

    Route::get('/users/{user}/tasks', [UserTaskController::class, 'index']);
    Route::apiResource('tasks', TaskController::class);
    Route::delete('tasks-hard/{task}', [TaskController::class, 'hardDestroy']);
});
