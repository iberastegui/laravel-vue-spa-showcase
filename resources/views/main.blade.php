<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>

<body class="antialiased">
    <div id='app' class="container">
        <div class="row justify-content-center">
            <router-link to='/tasks' title="Tasks" active><span>Tasks</span>
            </router-link> |
            <router-link to='/other-page' title="OtherPage"><span>OtherPage</span>
            </router-link>
        </div>
        <main class="content">
            <router-view />
        </main>
    </div>
</body>
<script src="{{ mix('/js/app.js') }}" defer></script>

</html>
