import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

let routes = [
    {
        path: "/tasks",
        name: "tasks",
        component: () => import("@/views/Tasks")
    },
    {
        path: "/other-page",
        name: "other-page",
        component: () => import("@/views/OtherPage")
    },
];

routes.push({
    path: "*",    
    component: () => import("@/views/Tasks")
});

const router = new VueRouter({
    base: "/",
    mode: "history",
    linkActiveClass: "font-bold",
    routes
});

export default router;
