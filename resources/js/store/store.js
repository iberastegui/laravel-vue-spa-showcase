import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        bearerToken:
            "vtcBEfMOFRElaqwiPNKj4r0RNwjQakXj1r7WTlqOPcAH7uJLFPlFFEAOMm1I", //obtained in register endpoint in postman collection
        register: {},
        url: "",
        errors: "",
        tasks: [],
    },
    mutations: {
        SET_TASKS: (state, payload) => (state.tasks = payload),
        SET_ERRORS: (state, payload) => (state.errors = payload),
    },
    actions: {
        getTasks(context) {
            axios
                .get(`/api/tasks`, {
                    headers: {
                        Authorization: `Bearer ${context.state.bearerToken}`,
                    },
                })
                .then(
                    (response) => {
                        context.commit("SET_TASKS", response.data.data);
                    },
                    (error) => {
                        console.log(error);
                    }
                );
        },
        storeTask(context, evt) {
            evt.preventDefault();
            console.log(context.state.register, "caca");
            axios
                .post(`/api/tasks`, context.state.register, {
                    headers: {
                        Authorization: `Bearer ${context.state.bearerToken}`,
                    },
                })
                .then(function (response) {
                    let tasks = context.state.tasks;
                    tasks.push(response.data.data);
                    context.commit("SET_TASKS", tasks);
                })
                .catch((error) => {
                    context.commit("SET_ERRORS", error.response.data.errors);
                });
        },

        deleteTask(context, { task, index }) {
            axios
                .delete(`/api/tasks/${task.id}`, {
                    headers: {
                        Authorization: `Bearer ${context.state.bearerToken}`,
                    },
                })
                .then((response) => {
                    let tasks = context.state.tasks;
                    tasks.splice(index, 1);
                    context.commit("SET_TASKS", tasks);
                });
        },
    },
});

export default store;
