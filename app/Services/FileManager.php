<?php
namespace App\Services;

use App\Models\File;
use App\Models\Task;
use Illuminate\Support\Facades\Storage;

class FileManager
{
    public static function upload(Task $task, $files)
    {
        if (!empty($files)) {
            if (!Storage::exists("/uploads")) {
                Storage::makeDirectory("/uploads");
            }

            foreach ($files as $file) {

                $fileName = $file->getClientOriginalName();
                $path = "/uploads/$task->id";
                File::create([
                    'path' => "$path/$fileName",
                    'task_id' => $task->id,
                ]);
                $file->storeAs($path, $fileName);

                //dd($path, $fileName);
            }
        }
    }

    public static function syncFiles(Task $task, $files)
    {
        if (!empty($files)) {
            foreach ($task->files as $file) {
                Storage::delete($file->path);
            }
            $task->files()->delete();
            self::upload($task, $files);
        }
    }
}
