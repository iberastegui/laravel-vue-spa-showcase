<?php

namespace App\Http\Controllers;

use App\Http\Resources\TaskResource;
use App\Models\User;

class UserTaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        return TaskResource::collection($user->tasks);

    }
}
